<?php

/**
 * WP implementation for refresh token repository
 *
 * @since      1.0.0
 *
 * @author     Luca Maroni <lcmaroni77@gmail.com>
 */
class Auth_Refresh_Token_WP_Repository implements Auth_Refresh_Token_Repository
{

    public function save_new_token($data)
    {
        global $wpdb;

        $now = new DateTime();

        return $wpdb->insert(
            $wpdb->prefix . 'jwt_refresh_tokens',
            array(
                'user_id' => $data['user_id'],
                'refresh_token' => $data['refresh_token'],
                'device_id' => $data['device_id'],
                'device_name' => $data['device_name'],
                'created_at' => $now->format('Y-m-d H:i:s'),
                'used_at' => NULL,
                'deleted_at' => NULL
            ),
            array(
                '%d',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s'
            )
        );
    }

    public function disable_token($token, $deviceId, $userId)
    {
        global $wpdb;

        $now = new DateTime();

        return $wpdb->update(
            $wpdb->prefix . 'jwt_refresh_tokens',
            array(
                'used_at' => $now->format('Y-m-d H:i:s')
            ),
            array(
                'user_id' => $userId,
                'refresh_token' => $token,
                'device_id' => $deviceId
            ),
            array('%s'),
            array('%d', '%s', '%s')
        );
    }

    public function delete_token($token, $deviceId, $userId)
    {
        global $wpdb;

        $now = new DateTime();

        return $wpdb->update(
            $wpdb->prefix . 'jwt_refresh_tokens',
            array(
                'deleted_at' => $now->format('Y-m-d H:i:s')
            ),
            array(
                'user_id' => $userId,
                'refresh_token' => $token,
                'device_id' => $deviceId
            ),
            array('%s'),
            array('%d', '%s', '%s')
        );
    }

    public function find_token($token, $deviceId = null, $userId = null)
    {
        global $wpdb;

        $sql = $wpdb->prepare("
          SELECT * 
          FROM " . $wpdb->prefix . "jwt_refresh_tokens 
          WHERE refresh_token = %s 
            AND device_id = %s 
            AND user_id = %s
            AND deleted_at IS NULL 
            AND used_at IS NULL
        ", $token, $deviceId, $userId);

        $check = $wpdb->get_row($sql);

        if (is_null($check)) {
            return false;
        }

        return $check;
    }
}
