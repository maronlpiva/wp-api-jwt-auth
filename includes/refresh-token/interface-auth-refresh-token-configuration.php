<?php
/**
 * Created by PhpStorm.
 * User: maronl
 * Date: 09/10/17
 * Time: 15:11
 */

/**
 * WP implementation for refresh token repository
 *
 * @since      1.0.0
 *
 * @author     Luca Maroni <lcmaroni77@gmail.com>
 */
interface Auth_Refresh_Token_Configuration
{
    public function get_ttl();
}