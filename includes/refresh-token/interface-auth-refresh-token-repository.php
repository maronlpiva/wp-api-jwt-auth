<?php

/**
 * The file that defines the core plugin class.
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://enriquechavez.co
 * @since      1.0.0
 */

/**
 * Interface to manage data for refresh token
 *
 * @since      1.0.0
 *
 * @author     Luca Maroni <lcmaroni77@gmail.com>
 */
interface Auth_Refresh_Token_Repository
{

    public function save_new_token($data);

    public function find_token($token, $deviceId, $userId);

    public function disable_token($token, $deviceId, $userId);

    public function delete_token($token, $deviceId, $userId);

}
