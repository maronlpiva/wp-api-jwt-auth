<?php

/**
 * WP implementation for refresh token configuration
 *
 * @since      1.0.0
 *
 * @author     Luca Maroni <lcmaroni77@gmail.com>
 */
class Auth_Refresh_Token_WP_Configuration implements Auth_Refresh_Token_Configuration
{

    public function get_ttl()
    {
        if(defined('JWT_AUTH_REFRESH_TOKEN_TTL')) {
            return (int) JWT_AUTH_REFRESH_TOKEN_TTL;
        }
        return 60 * 60 * 24 * 14;
    }

}
