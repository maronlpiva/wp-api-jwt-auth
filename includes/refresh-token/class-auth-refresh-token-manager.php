<?php

/**
 * The file that defines the core plugin class.
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://enriquechavez.co
 * @since      1.0.0
 */

//use Firebase\JWT\ExpiredException;

/**
 * Class to manage refresh token logic and data
 *
 * @since      1.0.0
 *
 * @author     Luca Maroni <lcmaroni77@gmail.com>
 */
class Auth_Refresh_Token_Manager
{

    private $repository;

    /**
     * @var Auth_Refresh_Token_Configuration
     */
    private $configuration;

    function __construct(Auth_Refresh_Token_Configuration $refreshTokenConfiguration, Auth_Refresh_Token_Repository $refreshTokenRepository)
    {
        $this->repository = $refreshTokenRepository;
        $this->configuration = $refreshTokenConfiguration;
    }

    public function generate_refresh_token()
    {
        return wp_generate_password(24) . '-' . wp_generate_password(12);
    }
    
    public function generate_user_refresh_token($user_id, $device_id = null, $device_name = null)
    {
        $newToken = $this->generate_refresh_token();

        if(!is_null($device_id) && !is_null($device_name)) {
            $data = array(
                'user_id' => $user_id,
                'device_id' => $device_id,
                'device_name' => $device_name,
                'refresh_token' => $newToken

            );

            $this->repository->save_new_token($data);
        }

        return $newToken;
    }

    public function is_valid_token($token, $deviceId = null, $userId = null)
    {
        $check = $this->repository->find_token($token, $deviceId, $userId);

        if(!$check) {
            throw new InvalidTokenException(__('The refresh token is not a valid token', 'wp-api-jwt-auth'));
        }

        $tokenTimestamp = strtotime($check->created_at);
        $nowTimestamp = strtotime(date('Y-m-d H:i:s'));

        if($this->configuration->get_ttl() <= (($nowTimestamp - $tokenTimestamp) / 60)) {
            throw new ExpiredTokenException(__('Expired token', 'wp-api-jwt-auth'));
        }

        return $check;
    }

}
