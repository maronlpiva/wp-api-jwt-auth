CREATE TABLE pld_jwt_refresh_tokens
(
  id BIGINT(11) AUTO_INCREMENT PRIMARY KEY,
  user_id BIGINT(11) NOT NULL,
  refresh_token VARCHAR(37) NOT NULL,
  device_id VARCHAR(100) NOT NULL,
  device_name VARCHAR(100) NOT NULL,
  created_at DATETIME DEFAULT '0000-00-00 00:00:00' NOT NULL,
  used_at DATETIME,
  deleted_at DATETIME
);

CREATE INDEX jwt_refresh_user_id ON pld_jwt_refresh_tokens(user_id);
CREATE INDEX jwt_refresh_device_id ON pld_jwt_refresh_tokens(device_id);
CREATE INDEX jwt_refresh_used_date ON pld_jwt_refresh_tokens(used_at);
CREATE INDEX jwt_refresh_deleted_date ON pld_jwt_refresh_tokens(deleted_at);
